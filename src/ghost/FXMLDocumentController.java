/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ghost;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.text.Text;

/**
 *
 * @author CSIE
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Label label;
    @FXML
    private Button button;
    @FXML
    private Label image1;
    @FXML
    private Label image2;
    @FXML
    private Label image3;
     private String sss[]={"☣","☯","☪"};
    @FXML
    private Text lab1;
    @FXML
    private Text lab2;
    @FXML
    private Text lab3;
    @FXML
    private Button re;
    @FXML
    private Button add;
     private int a,b;
    @FXML
    private Text sum;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        // System.out.println("You clicked me!");
        // label.setText("Hello World!");
        Random ran = new Random();
        image1.setText(sss[ran.nextInt(3)]);
       image2.setText(sss[ran.nextInt(3)]);
        image3.setText(sss[ran.nextInt(3)]);
        if((image1.getText()=="☯"&&image2.getText()=="☯"&&image3.getText()=="☯")||(image1.getText()=="☣"&&image2.getText()=="☣"&&image3.getText()=="☣")||(image1.getText()=="☪"&&image2.getText()=="☪"&&image3.getText()=="☪"))
        {
            a=Integer.parseInt(lab2.getText());
            b=a;
            switch(image1.getText()){
                
                case "☣":
                    b*=3.84;
                    break;
                case "☪":
                    b*=1.92;
                    break;
                case"☯":
                    b*=1.92;
                    break;
            
            }
           
         
            lab1.setText(String.valueOf(b));
            
            sum.setText(String.valueOf(Integer.parseInt(sum.getText())+b));
        }
        else
        {
        a=Integer.parseInt(sum.getText());
        a-=Integer.parseInt(lab2.getText());
        sum.setText(String.valueOf(a));
        lab1.setText("0");
        }
            
        
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void handlereAction(ActionEvent event) {
        if(Integer.parseInt(lab2.getText())>0)
        {
          a=Integer.parseInt(lab2.getText());
          a-=100;
          lab2.setText(String.valueOf(a));
        }
        
    }

    @FXML
    private void handleaddAction(ActionEvent event) {
        if(Integer.parseInt(lab2.getText())<=Integer.parseInt(sum.getText()))
        {
          a=Integer.parseInt(lab2.getText());
          a+=100;
          lab2.setText(String.valueOf(a));
        }
    }

}
